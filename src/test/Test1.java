package test;
import main.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class Test1 {

	RegularFile f1 = new RegularFile("file1", 3);
	RegularFile f2 = new RegularFile("file2", 4);
	Directory d1 = new Directory("dir1");
	RegularFile f3 = new RegularFile("file3", 5);
	Directory d2 = new Directory("dir2");
	Directory d3 = new Directory("dir3");

	
	@Test
	public void testTaille() {


		try {
			d1.addSon(f1);
			d1.addSon(f2);
			d2.addSon(f3);
			assertEquals(7,d1.getTaille());
			d2.addSon(d1);
			assertEquals(12,d2.getTaille());
		} catch (InfiniteLoopException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test(expected=InfiniteLoopException.class)
	public void testBoucles() throws InfiniteLoopException{
		d1.addSon(f1);
		d1.addSon(f2);
		d2.addSon(f3);
		d2.addSon(d1);
		d1.addSon(d2);
	}
	@Test(expected=InfiniteLoopException.class)
	public void autoAdd() throws InfiniteLoopException{
		d1.addSon(d1);
	}
	@Test
	public void testDescendence(){
		try {
			d1.addSon(f1);
			d1.addSon(f2);
			d2.addSon(f3);
			d2.addSon(d1);
			//d1.addSon(d2);
			assertFalse(Directory.isDescendent(d2, d1));
			d3.addSon(d2);
			assertTrue(Directory.isDescendent(d2, d3));
			assertTrue(Directory.isDescendent(d1, d2));
			assertTrue(Directory.isDescendent(d1, d3));
		} catch (InfiniteLoopException e) {
			System.out.print("Unexpected InfiniteLoopException");
		}
	}
	

}

