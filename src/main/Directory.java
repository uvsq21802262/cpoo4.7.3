package main;

import java.util.ArrayList;


public class Directory extends FileDescriptor{
	
	
	/**
	 * L'ensemble des sous r�pertoires et des fichiers 
	 */
	ArrayList<FileDescriptor> sons;
	
	/**
	 * @param name : Initialisation d'un r�pertoire avec son nom en param�tre
	 */
	public Directory(String name) {
		super(name);
		sons = new ArrayList<FileDescriptor>();
		super.size = 0;
	}
	
	
	/**
	 * Cette fonction v�rifie si le r�pertoire � ajouter correspond � un r�pertoire parent
	 * @param source : r�pertoire parent
	 * @param toAdd : r�pertoire � ajouter
	 * @return : true si le r�pertoire � ajouter correspond � un r�pertoire parent
	 * false sinon
	 */
	public static boolean isDescendent(Directory source, Directory toAdd){
		System.out.println("\tDebut : isDescendant : "+source.name+" | "+toAdd.name);
		if(source == toAdd){
			return true;
		}
		if(source.father == null){
			return false;
		}
		return isDescendent(source.father, toAdd);
	}
	
	
	/**
	 * @param f Descripteur de fichier 
	 * @throws InfiniteLoopException
	 */
	public void addSon(FileDescriptor f) throws InfiniteLoopException{
		System.out.println("AJOUT DE "+f.name+" DANS "+this.name);
		if( f instanceof Directory){
			if(isDescendent(this, (Directory)f)){
				throw new InfiniteLoopException(); //Pourquoi Infinite loop Exception et pas Exception tout court ?
			}
		}
		sons.add(f);
		f.father = this;
	}

	
	/* (non-Javadoc)
	 * @see main.FileDescriptor#getTaille()
	 */
	public int getTaille(){
		int taille = 0;
		for(FileDescriptor f:sons){
			taille = taille + f.getTaille();
		}
		return taille;
	}


	@Override
	public String toString() {
		return "Directory [sons=" + sons + ", name=" + name + ", father=" + father + ", size=" + size + ", getTaille()="
				+ getTaille() + ", toString()=" + this.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}
}

