package main;

public class RegularFile extends FileDescriptor{
	private int size;
	public RegularFile(String name, int taille) {
		super(name);
		this.size = taille;
	}
	@Override
	public int getTaille(){
		return size;
	}
	@Override
	public String toString(){
		return name;
	}
	
}
