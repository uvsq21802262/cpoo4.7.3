package main;

public abstract class FileDescriptor {
	protected String name;
	protected Directory father;
	protected int size;
	public FileDescriptor(String name){
		this.name = name;
		father = null;
		
	}
	
	/**
	 * @return : Retourne la taille totale du répertoire.
	 */
	public abstract int getTaille();
	
	public abstract String toString();
	
}
